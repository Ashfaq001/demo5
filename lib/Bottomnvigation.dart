import 'package:demo5/Community.dart';
import 'package:demo5/Event.dart';
import 'package:demo5/dealsoftheday.dart';
import 'package:demo5/dealsofthedayiN.dart';
import 'package:demo5/home.dart';
import 'package:flutter/material.dart';

class BottomNavScreen extends StatelessWidget {
  const BottomNavScreen({Key? key}) : super(key: key);

  // static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      // title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  var list = [
    const HomeScreen(),
    const EventScreen(),
    const DealofTheDay(),
    const CommunityScreen(),
    const CommunityScreen(),
  ];

  get _onBackPressed => null;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.black,
          bottomNavigationBar: ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(40.0),
              topRight: Radius.circular(40.0),
            ),
            child: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.width * .16,
              child: BottomNavigationBar(
                elevation: 30,
                type: BottomNavigationBarType.fixed,
                currentIndex: _selectedIndex,
                iconSize: 26,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.white,
                unselectedItemColor: const Color.fromRGBO(109, 109, 109, 109),
                selectedLabelStyle: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.03,
                ),
                showUnselectedLabels: true,
                onTap: (list) {
                  setState(() => _selectedIndex = list);
                },
                items: const [
                  BottomNavigationBarItem(
                    label: "Home",
                    icon: Icon(Icons.home),
                  ),
                  BottomNavigationBarItem(
                    label: "Event",
                    icon: Icon(Icons.event),
                  ),
                  BottomNavigationBarItem(
                    label: "Market",
                    icon: Icon(Icons.shop),
                  ),
                  BottomNavigationBarItem(
                    label: "Community",
                    icon: Icon(Icons.people_alt_outlined),
                  ),
                  BottomNavigationBarItem(
                    label: "LeaderBoard",
                    icon: Icon(Icons.leaderboard),
                  ),
                ],
              ),
            ),
          ),
          body: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: const BoxDecoration(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(50))),
              ),
              list[_selectedIndex],
            ],
          ),
        ),
      ),
    );
  }
}

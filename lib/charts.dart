import 'package:flutter/material.dart';

class Charts extends StatefulWidget {
  const Charts({Key? key}) : super(key: key);

  @override
  _ChartsState createState() => _ChartsState();
}

class _ChartsState extends State<Charts> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text(
            'Your Fitness Activity',
          ),
          centerTitle: true,
          bottom: const TabBar(
            indicatorColor: Colors.white,
            indicatorWeight: 5.0,
            labelColor: Colors.white,
            labelPadding: EdgeInsets.only(
              top: 10.0,
            ),
            unselectedLabelColor: Colors.grey,
            tabs: [
              Tab(
                text: 'DAY',
              ),
              //child: Image.asset('images/android.png'),

              Tab(
                text: 'WEEK',
                // icon: Icon(
                //   Icons.radio,
                //   color: Colors.white,
                // ),
                // iconMargin: EdgeInsets.only(bottom: 10.0),
              ),
              Tab(
                text: 'MONTH',

                // icon: Icon(
                //   Icons.card_giftcard,
                //   color: Colors.white,
                // ),
                // iconMargin: EdgeInsets.only(bottom: 0.0),
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            Center(
                child: Text(
              'NO DATA!!!',
              style: TextStyle(fontSize: 32, color: Colors.white),
            )),
            Center(
                child: Text(
              'NO DATA!!!',
              style: TextStyle(fontSize: 32, color: Colors.white),
            )),
            Center(
                child: Text(
              'NO DATA!!!',
              style: TextStyle(fontSize: 32, color: Colors.white),
            )),
          ],
        ),
      ),
    );
  }
}

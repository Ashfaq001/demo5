import 'package:flutter/material.dart';

class ChatAddScreen extends StatefulWidget {
  const ChatAddScreen({Key? key}) : super(key: key);

  @override
  _ChatAddScreenState createState() => _ChatAddScreenState();
}

class _ChatAddScreenState extends State<ChatAddScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
            title: const Text("Community"),
            centerTitle: true,
            backgroundColor: Colors.black,
            shadowColor: Colors.grey,
            bottom: const TabBar(
              tabs: <Widget>[
                // Tab(text: "Chat box"),
                Tab(text: "New Chat"),
                Tab(text: "New group"),
              ],
            )),
        body: TabBarView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 45.0,
                          width: MediaQuery.of(context).size.width * .82,
                          child: TextField(
                            onTap: () {},
                            style: const TextStyle(
                                fontSize: 17,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(7.0),
                                  ),
                                ),
                                filled: true,
                                hintStyle: TextStyle(color: Colors.white),
                                hintText: "Find friend",
                                fillColor: Colors.grey),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * .11,
                          height: 42,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: InkWell(
                            onTap: () {},
                            child: const Icon(
                              Icons.search,
                              size: 30,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 45.0,
                        width: MediaQuery.of(context).size.width * .82,
                        child: TextField(
                          onTap: () {},
                          style: const TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(7.0),
                                ),
                              ),
                              filled: true,
                              hintStyle: TextStyle(color: Colors.white),
                              hintText: "Find friend",
                              fillColor: Colors.grey),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * .11,
                        height: 42,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: InkWell(
                          onTap: () {},
                          child: const Icon(
                            Icons.search,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class DealsofTheDayiN extends StatefulWidget {
  const DealsofTheDayiN({Key? key}) : super(key: key);

  @override
  _DealsofTheDayiNState createState() => _DealsofTheDayiNState();
}

class _DealsofTheDayiNState extends State<DealsofTheDayiN> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(color: Colors.white),
          actions: [
            Row(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.archive_outlined,
                    // size: 28,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.share,
                    // size: 28,
                  ),
                )
              ],
            ),
          ],
        ),
        backgroundColor: Colors.black,
        body: SingleChildScrollView(
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                "assets/images/ftball.jpg",
                fit: BoxFit.fill,
                width: 420,
                height: 250,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  const Icon(
                    Icons.view_compact_rounded,
                    color: Colors.white,
                    size: 36,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Column(
                      children: [
                        const Text(
                          'IVRAH',
                          style: TextStyle(
                            // decoration: TextDecoration.underline,
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        InkWell(
                          onTap: () {},
                          child: const Text(
                            'Contact Us',
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 210),
                    child: Icon(
                      Icons.thumb_up,
                      color: Colors.grey,
                      // size: 28,
                    ),
                  ),
                  const Icon(
                    Icons.thumb_down,
                    color: Colors.grey,
                    // size: 28,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 14, left: 12),
              child: Row(
                children: const [
                  Text(
                    'Flat 20% Off at ivrah',
                    style: TextStyle(
                        // decoration: TextDecoration.underline,
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 5),
              child: Row(
                children: const [
                  Text(
                    'Get flat 20% Off on your favorite design from  \nivrah. Choose from the latest catalog of the\nmost trending footwear . we keep in mind conftable...',
                    style: TextStyle(
                      // decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12, top: 10),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)),
                          color: Colors.blueAccent,
                          child: Row(
                            children: const [
                              Text(
                                "Discount",
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ),
                          onPressed: () {}),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14),
                          ),
                          color: Colors.grey,
                          child: Row(
                            children: const [
                              Text(
                                "ivrah*stepsetgo Exclucive",
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14)),
                      color: Colors.grey,
                      child: Row(
                        children: const [
                          Text(
                            "Premium footwear",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {}),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14),
                      ),
                      color: Colors.grey,
                      child: Row(
                        children: const [
                          Text(
                            "Coupon expiry-20th jan,2022",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   width: MediaQuery.of(context).size.width * 1,
            //   height: MediaQuery.of(context).size.hashCode *.69,
            //   padding: const EdgeInsets.all(20.0),
            //   decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(30.0),
            //       border: Border(left: BorderSide()),
            //       color: Colors.grey[800]),

            Container(
              margin: const EdgeInsets.symmetric(vertical: 20.0),
              height: 200.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      child: InkWell(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.asset(
                            "assets/images/ftball.jpg",
                            fit: BoxFit.fill,
                            width: 350,
                            height: 300,
                          ),
                        ),
                      ),
                      width: 160.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      child: InkWell(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.asset(
                            "assets/images/ftball.jpg",
                            fit: BoxFit.fill,
                            width: 350,
                            height: 300,
                          ),
                        ),
                      ),
                      width: 160.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      child: InkWell(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.asset(
                            "assets/images/ftball.jpg",
                            fit: BoxFit.fill,
                            width: 350,
                            height: 300,
                          ),
                        ),
                      ),
                      width: 160.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      child: InkWell(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.asset(
                            "assets/images/ftball.jpg",
                            fit: BoxFit.fill,
                            width: 350,
                            height: 300,
                          ),
                        ),
                      ),
                      width: 160.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      child: InkWell(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.asset(
                            "assets/images/ftball.jpg",
                            fit: BoxFit.fill,
                            width: 350,
                            height: 300,
                          ),
                        ),
                      ),
                      width: 160.0,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Row(
                children: const [
                  Text(
                    'about sko',
                    style: TextStyle(
                        // decoration: TextDecoration.underline,
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 5),
              child: Row(
                children: const [
                  Text(
                    'Get flat 20% Off on your favorite design from  \nivrah. Choose from the latest catalog of the\nmost trending footwear . we keep in mind conftable...',
                    style: TextStyle(
                      // decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: const [
                              Text(
                                'Tell me more!',
                                style: TextStyle(
                                    // decoration: TextDecoration.underline,
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 0),
                          child: Row(
                            children: const [
                              Text(
                                'more about sko',
                                style: TextStyle(
                                  // decoration: TextDecoration.underline,
                                  color: Colors.white,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14),
                        ),
                        color: Colors.white,
                        child: Row(
                          children: const [
                            Text(
                              "view more",
                              style: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                // width: 300,
                height: 60,
                padding: const EdgeInsets.all(0.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    border: Border.all(color: Colors.grey),
                    color: Colors.black),
              ),
            ),
            const Divider(
              color: Colors.grey,
              thickness: 0.3,
            ),

            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: const [
                  Text(
                    "more from SKO",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 400,
              child: GridView.count(
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                scrollDirection: Axis.vertical,
                crossAxisCount: 3,
                children: [
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(28),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(28),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Image.asset(
                          "assets/images/ftball.jpg",
                          fit: BoxFit.fill,
                          //
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Divider(
              color: Colors.grey,
              thickness: 0.3,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 12),
              child: Row(
                children: const [
                  Text(
                    'about sko',
                    style: TextStyle(
                        // decoration: TextDecoration.underline,
                        color: Colors.white,
                        fontSize: 22,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 10, bottom: 5),
              child: Row(
                children: const [
                  Text(
                    'Other who cliamed this offer',
                    style: TextStyle(
                      // decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 400,
              child: GridView.count(
                crossAxisCount: 4,
                children: [
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(26.0),
                              border: Border.all(
                                  color: Colors.yellow.shade700, width: 2),
                            ),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                  Card(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(26.0),
                                border:
                                    Border.all(color: Colors.yellow, width: 2),
                                color: Colors.black),
                            child: ClipOval(
                              child: Image.asset(
                                "assets/images/ftball.jpg",
                              ),
                            ),
                          ),
                        ),
                        const Text(
                          'Ram',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          '@Ramlaa1010',
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.grey[700],
                  ),
                ],
              ),
            ),
          ]),
        ));
  }
}

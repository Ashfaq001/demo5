// ignore_for_file: deprecated_member_use

import 'package:demo5/Bottomnvigation.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:country_state_city_picker/country_state_city_picker.dart';

class nextScreen extends StatefulWidget {
  // const nextScreen({Key? key, this.title}) : super(key: key);
  // final  title;
  @override
  _nextScreenState createState() => _nextScreenState();
}

// ignore: camel_case_types
class _nextScreenState extends State<nextScreen> {
  String dropdownValue = 'Male';
  int _currentValue = 10;
  int _weight = 10;
  int _height = 10;

  String countryValue = "";
  String stateValue = "";
  String cityValue = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.black,
          title: const Text(
            "Create",
          ),
        ),
        body: Center(
            child: SingleChildScrollView(
                child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * .90,
                    height: 100,
                    child: RaisedButton(
                      color: Colors.white24,
                      onPressed: () {},
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_drop_down),
                        elevation: 16,
                        style: const TextStyle(color: Colors.black),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: <String>[
                          'Select Gender',
                          'Male',
                          'Female',
                          'others',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: TextStyle(fontSize: 18),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 20),
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  onPressed: () {},
                  child: Container(
                    width: MediaQuery.of(context).size.width * .90,
                    color: Colors.transparent,
                    height: 100,
                    child: Row(
                      children: [
                        NumberPicker(
                          value: _currentValue,
                          minValue: 10,
                          maxValue: 100,
                          onChanged: (value) =>
                              setState(() => _currentValue = value),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 100.0),
                          child: Text(
                            'Age: $_currentValue',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 20),
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  onPressed: () {},
                  child: Container(
                    width: MediaQuery.of(context).size.width * .90,
                    color: Colors.transparent,
                    height: 100,
                    child: Row(
                      children: [
                        NumberPicker(
                          value: _weight,
                          minValue: 10,
                          maxValue: 100,
                          onChanged: (value) => setState(() => _weight = value),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 100.0),
                          child: Text(
                            'Weight: $_weight',
                            style: const TextStyle(fontSize: 20),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 5.0),
                          child: Text(
                            "KG",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 20),
                child: RaisedButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  onPressed: () {},
                  child: Container(
                    width: MediaQuery.of(context).size.width * .90,
                    color: Colors.transparent,
                    height: 100,
                    child: Row(
                      children: [
                        NumberPicker(
                          value: _height,
                          minValue: 10,
                          maxValue: 100,
                          onChanged: (value) => setState(() => _height = value),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 95.0),
                          child: Text(
                            'Height: $_height',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 5.0),
                          child: Text(
                            "inch",
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
              child: TextField(
                decoration: InputDecoration(
                  labelText: "Activity",
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0, left: 30, right: 30),
              child: SelectState(
                onCountryChanged: (value) {
                  setState(() {
                    countryValue = value;
                  });
                },
                onStateChanged: (value) {
                  setState(() {
                    stateValue = value;
                  });
                },
                onCityChanged: (value) {
                  setState(() {
                    cityValue = value;
                  });
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white, width: 2),
                    borderRadius: BorderRadius.circular(14),
                  ),
                  color: Colors.black,
                  child: const Text(
                    "NEXT",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BottomNavScreen()));
                  }),
            ),
          ]),
        ))));
  }
}

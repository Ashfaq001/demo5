// ignore_for_file: deprecated_member_use

import 'package:demo5/charts.dart';
import 'package:demo5/settings.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
              title: const Text("PROFILE"),
              centerTitle: true,
              backgroundColor: Colors.black,
              elevation: 0,
              bottom: const TabBar(
                tabs: <Widget>[
                  Tab(text: "PROFILE"),
                  Tab(text: "ACTIVITY"),
                ],
              )),
          body: TabBarView(
            children: <Widget>[
              Center(
                child: Column(
                  children: [
                    Image.asset(
                      "assets/images/cam1.jpg",
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Settings()));
                          },
                          icon: const Icon(
                            Icons.settings,
                            // size: 72,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // ignore: sized_box_for_whitespace
                          Container(
                            height: 50,
                            width: 110,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  side: const BorderSide(
                                      color: Colors.white, width: 2),
                                  borderRadius: BorderRadius.circular(12)),
                              color: Colors.transparent,
                              child: const Text(
                                "Total Steps",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.white),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (ctx) => const Charts()));
                              },
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 110,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                side: const BorderSide(
                                    color: Colors.white, width: 2),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              color: Colors.transparent,
                              child: const Text(
                                "Total Coins Earned",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.white),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (ctx) => const Charts()));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, left: 24, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(
                            "Your Coins",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                          Text(
                            "0.00",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(left: 22.0, top: 10),
                          child: Text(
                            "Current balance",
                            style: TextStyle(fontSize: 16, color: Colors.grey),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 35.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    side: const BorderSide(
                                        color: Colors.white, width: 2),
                                    borderRadius: BorderRadius.circular(10)),
                                color: Colors.grey[400],
                                child: const Text("View transactions"),
                                onPressed: () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => const Screen2()));
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    side: const BorderSide(
                                        color: Colors.white, width: 2),
                                    borderRadius: BorderRadius.circular(10)),
                                color: Colors.lightBlue,
                                child: const Text("Spend Coins"),
                                onPressed: () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => const Screen3()));
                                }),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/g2.jpg",
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 100.0),
                    child: Text(
                      "ADD OTHERS!!! ",
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }
}

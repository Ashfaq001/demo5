// ignore_for_file: deprecated_member_use

import 'package:demo5/entercode.dart';
import 'package:demo5/next.dart';
import 'package:demo5/register.dart';
import 'package:demo5/screen2.dart';
import 'package:demo5/screen3.dart';
import 'package:flutter/material.dart';

class Screen1 extends StatefulWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  _Screen1State createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * .60,
                  width: MediaQuery.of(context).size.width * 1,
                  child: Image.asset(
                    "assets/images/run.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .60,
                  width: MediaQuery.of(context).size.width * 1,
                  child: Image.asset(
                    "assets/images/run.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .60,
                  width: MediaQuery.of(context).size.width * 1,
                  child: Image.asset(
                    "assets/images/run.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .60,
                  width: MediaQuery.of(context).size.width * 1,
                  child: Image.asset(
                    "assets/images/run.jpg",
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Text(
              "CHALLENGE YOURSELF IN THE ARENA",
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.lightBlue,
                  fontWeight: FontWeight.w500),
            ),
          ),
          // const Padding(
          //   padding: EdgeInsets.only(top: 10.0),
          //   child: Text(
          //     "Join curated challenges,\n  get fitter,get rewarded.",
          //     style: TextStyle(
          //         fontSize: 16,
          //         color: Colors.white,
          //         fontWeight: FontWeight.w300),
          //   ),
          // ),
          // const SizedBox(
          //   height: 40,
          // ),
          const Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Text(
              "By continuing you to agree our",
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.w300),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: InkWell(
              child: Text(
                "Terms & conditions & privacy",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.only(top: 35.0),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       Padding(
          //         padding: const EdgeInsets.only(left: 20.0),
          //         child: RaisedButton(
          //             shape: RoundedRectangleBorder(
          //                 side: BorderSide(color: Colors.white, width: 2),
          //                 borderRadius: BorderRadius.circular(20)),
          //             color: Colors.grey[400],
          //             child: const Text(
          //               "Sign in",
          //               style: TextStyle(
          //                 fontWeight: FontWeight.bold,
          //                 fontSize: 16,
          //               ),
          //             ),
          //             onPressed: () {
          //               Navigator.push(
          //                   context,
          //                   MaterialPageRoute(
          //                       builder: (context) => const Screen2()));
          //             }),
          //       ),
          Padding(
            padding: const EdgeInsets.only(top: 40, right: 0.0),
            child: Container(
              width: MediaQuery.of(context).size.width * .90,
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      // side: BorderSide(color: Colors.white, width: 2),
                      borderRadius: BorderRadius.circular(8)),
                  color: Colors.white,
                  child: const Text(
                    "REGISTER",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Register()));
                  }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text(
                  "Invited by a Friend",
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w300),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: Text(
                    "Already a User",
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0, top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Entercode()));
                  },
                  child: const Text(
                    "Enter Code",
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                InkWell(
                  onTap: () {
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => nextScreen()));
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: Text(
                      "Log In",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

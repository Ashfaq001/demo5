// // ignore_for_file: deprecated_member_use

// import 'package:demo5/next.dart';
// import 'package:demo5/screen3.dart';
// import 'package:flutter/material.dart';

// class Screen2 extends StatefulWidget {
//   const Screen2({Key? key}) : super(key: key);

//   @override
//   _Screen2State createState() => _Screen2State();
// }

// class _Screen2State extends State<Screen2> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         backgroundColor: Colors.black,
//         title: const Text(
//           "Sign in",
//         ),
//       ),
//       backgroundColor: Colors.black,
//       body: Padding(
//         padding: const EdgeInsets.only(top: 50.0),
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   SizedBox(
//                     height: 50,
//                     width: 250,
//                     child: RaisedButton(
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(14)),
//                         color: Colors.blueAccent,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           children: [
//                             SizedBox(
//                               height: 25,
//                               width: 25,
//                               child: Image.asset("assets/images/google.png"),
//                             ),
//                             const Text(
//                               "Sign in with google",
//                               style: TextStyle(
//                                   fontSize: 16, fontWeight: FontWeight.bold),
//                             ),
//                           ],
//                         ),
//                         onPressed: () {}),
//                   ),
//                 ],
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               SizedBox(
//                 height: 50,
//                 width: 250,
//                 child: RaisedButton(
//                     shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(14)),
//                     color: Colors.blueAccent,
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         SizedBox(
//                           height: 35,
//                           width: 35,
//                           child: Image.asset("assets/images/facebook.png"),
//                         ),
//                         const Text(
//                           "Sign in with facebook",
//                           style: TextStyle(
//                               fontSize: 16, fontWeight: FontWeight.bold),
//                         ),
//                       ],
//                     ),
//                     onPressed: () {}),
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               SizedBox(
//                 height: 50,
//                 width: 250,
//                 child: RaisedButton(
//                     shape: RoundedRectangleBorder(
//                       side: const BorderSide(color: Colors.white, width: 2),
//                       borderRadius: BorderRadius.circular(14),
//                     ),
//                     color: Colors.transparent,
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: const [
//                         SizedBox(
//                           height: 35,
//                           width: 35,
//                           child: Icon(
//                             Icons.email,
//                             color: Colors.white,
//                           ),
//                         ),
//                         Text(
//                           "Sign in with email",
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 16,
//                               fontWeight: FontWeight.bold),
//                         ),
//                       ],
//                     ),
//                     onPressed: () {}),
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   const Text(
//                     'New User?',
//                     style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 16,
//                         fontWeight: FontWeight.w300),
//                   ),
//                   InkWell(
//                     onTap: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => nextScreen()));
//                     },
//                     child: const Text(
//                       'Create Account',
//                       style: TextStyle(
//                           decoration: TextDecoration.underline,
//                           color: Colors.white,
//                           fontSize: 16,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ],
//               ),
//               const SizedBox(
//                 height: 300,
//               ),
//               const Text(
//                 'Can`t remember your email address?',
//                 style: TextStyle(

//                     // decoration: TextDecoration.underline,
//                     color: Colors.white,
//                     fontSize: 16,
//                     fontWeight: FontWeight.w300),
//               ),
//               InkWell(
//                 onTap: () {},
//                 child: const Text(
//                   'Recover Account',
//                   style: TextStyle(
//                       decoration: TextDecoration.underline,
//                       color: Colors.white,
//                       fontSize: 16,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//               const Text(
//                 'Having Trouble logging in?',
//                 style: TextStyle(
//                     // decoration: TextDecoration.underline,
//                     color: Colors.white,
//                     fontSize: 16,
//                     fontWeight: FontWeight.w300),
//               ),
//               InkWell(
//                 onTap: () {},
//                 child: const Text(
//                   'Contact Us',
//                   style: TextStyle(
//                       decoration: TextDecoration.underline,
//                       color: Colors.white,
//                       fontSize: 16,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

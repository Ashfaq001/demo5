// // ignore_for_file: deprecated_member_use

// import 'package:demo5/next.dart';
// import 'package:flutter/material.dart';

// class Screen3 extends StatefulWidget {
//   const Screen3({Key? key}) : super(key: key);

//   @override
//   _Screen3State createState() => _Screen3State();
// }

// class _Screen3State extends State<Screen3> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         backgroundColor: Colors.black,
//         title: const Text(
//           "Create Account",
//         ),
//       ),
//       // backgroundColor: Colors.black,
//       body: Padding(
//         padding: const EdgeInsets.only(top: 50.0),
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     labelText: "Firstname",
//                     fillColor: Colors.white,
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         color: Colors.black,
//                       ),
//                       borderRadius: BorderRadius.circular(25.0),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     labelText: "Lastname",
//                     fillColor: Colors.white,
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         color: Colors.black,
//                       ),
//                       borderRadius: BorderRadius.circular(25.0),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
//                 child: TextField(
//                   keyboardType: TextInputType.number,
//                   decoration: InputDecoration(
//                     labelText: "phone",
//                     fillColor: Colors.white,
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         color: Colors.black,
//                       ),
//                       borderRadius: BorderRadius.circular(25.0),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     labelText: "password",
//                     fillColor: Colors.white,
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         color: Colors.black,
//                       ),
//                       borderRadius: BorderRadius.circular(25.0),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
//                 child: TextField(
//                   decoration: InputDecoration(
//                     labelText: "Confirm password",
//                     fillColor: Colors.white,
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         color: Colors.black,
//                       ),
//                       borderRadius: BorderRadius.circular(25.0),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 20.0),
//                 child: RaisedButton(
//                     shape: RoundedRectangleBorder(
//                       side: BorderSide(color: Colors.white, width: 2),
//                       borderRadius: BorderRadius.circular(14),
//                     ),
//                     color: Colors.black,
//                     child: const Text(
//                       "NEXT",
//                       style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 16,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     onPressed: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => nextScreen()));
//                     }),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

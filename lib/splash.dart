import 'package:demo5/screen1.dart';
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      data();
      // Navigator.push(context, MaterialPageRoute(builder: (ctx) => Profile()));
    });
  }

  data() async {
    var token = await gettoken();
    // ignore: avoid_print
    print(token);
    if (token != null) {
      // Navigator.push(context, MaterialPageRoute(builder: (ctx) => Profile()));
    }
    if (token == null) {
      Navigator.push(
          context, MaterialPageRoute(builder: (ctx) => const Screen1()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.black,
        title: const Text(
          "Welcome to StepSetGo",
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: const Center(
        child: Text(
          "DEMO PROJECT",
          style: TextStyle(color: Colors.white, fontSize: 24),
        ),
      ),
    );
  }

  gettoken() {}
}
